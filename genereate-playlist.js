const fs = require('fs')
const glob = require('glob')
const slugify = require('slugify')

const slugifyFilenameOption = {
  strict: true,
  lower: true,
}
const slugifyFilename = (title) => {
  slugify.extend({ ß: 'eszett' })
  slugify.extend({ '☢': 'radioactive' })
  return slugify(title, slugifyFilenameOption)
}
const getDirectories = function (src, callback) {
  glob(src + '/**/*.*', callback)
}
const getFileList = () => {
  return new Promise((resolve) => {
    getDirectories('./static/audio/', function (err, res) {
      if (err) {
        console.log('Error', err)
      } else {
        resolve(res)
      }
    })
  })
}

const generateSongs = async () => {
  const files = await getFileList()
  const songs = []
  files.forEach((file) => {
    const url = file.replace('./static/audio/', '/audio/')
    const filenamepart = file.split('/')
    const filename = filenamepart[filenamepart.length - 1]
    songs.push({
      name: filename,
      url,
    })
  })
  return songs
}
const generatePlaylists = (songs) => {
  const playlists = {}
  songs.forEach((song, index) => {
    const parts = song.url.split('/')
    const title = parts[parts.length - 2]
    const slug = slugifyFilename(title)

    if (!playlists[slug]) {
      playlists[slug] = {
        songs: [],
        title,
      }
    }

    playlists[slug].songs.push(index)
  })
  return playlists
}
const generateAmplitudeData = async () => {
  const songs = await generateSongs()
  const playlists = generatePlaylists(songs)
  const amplitudeData = {
    songs,
    playlists,
  }
  fs.writeFileSync(
    './assets/playlist.json',
    JSON.stringify(amplitudeData, null, 2)
  )
}

generateAmplitudeData()
